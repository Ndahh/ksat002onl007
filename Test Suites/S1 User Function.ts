<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>S1 User Function</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>76d91afc-a5f2-4485-986d-1c14c66412bc</testSuiteGuid>
   <testCaseLink>
      <guid>278693cb-a982-41c2-a397-8ab299b301de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Blocks/02 Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ff5ab634-5a58-4e2a-93ff-4a7cf115a5ba</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/account_list</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>ff5ab634-5a58-4e2a-93ff-4a7cf115a5ba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>547cff13-8da1-4117-85ba-46e861f2c8db</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ff5ab634-5a58-4e2a-93ff-4a7cf115a5ba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>fb0494ed-6974-476d-9505-4bc95af5733e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
