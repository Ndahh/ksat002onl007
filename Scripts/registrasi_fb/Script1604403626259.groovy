import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.openBrowser('')

WebUI.navigateToUrl('https://web.facebook.com/?_rdc=1&_rdr')

WebUI.click(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/button_register'))

WebUI.setText(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/input_firstName'), 'Endang')

WebUI.setText(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/input_lastName'), 'Hermawati')

String email = CustomKeywords.'com.ksat002.function.util.StringUtil.getRandomEmail'()

WebUI.setText(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/input_email'), email)

WebUI.setText(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/confirm_email'), email)

WebUI.setEncryptedText(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/input_password'), '1H8giTfeDT2ZljbKq6U2Rw==')

WebUI.selectOptionByValue(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/select_date'), '20', true)

WebUI.selectOptionByValue(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/select_year'), '2001', true)

WebUI.click(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/select_gender'))

WebUI.click(findTestObject('Facebook/Page_Facebook - Masuk atau Daftar/confirm_register'))

